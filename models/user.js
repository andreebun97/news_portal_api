var mysql_config = require('../config/database/mysql.json');

module.exports = {
    get_connection: async function(){
        var mysql = require('mysql2/promise');
        var bluebird = require('bluebird');
        
        var connection = await mysql.createConnection({
            host: mysql_config.host,
            user: mysql_config.user,
            password: mysql_config.password,
            database: mysql_config.database,
            Promise: bluebird
        });
        
        return connection;
    },
    get_user_data: async function(username,imei,email){
        var connection = await this.get_connection();
        var [result] = [];
        if(imei == null && email == null){
            [result] = await connection.execute('SELECT * FROM users JOIN roles ON roles.role_id = users.role_id WHERE username = "'+username+'"');
        }else{
            [result] = await connection.execute('SELECT * FROM users JOIN roles ON roles.role_id = users.role_id WHERE username = "'+username+'" OR imei = "'+imei+'" OR email = "'+email+'"');
        }

        connection.end();
        return [result];
    },
    insert_user_data: async function(fullname,email,username,password,imei,role_id){
        var connection = await this.get_connection();

        var [result] = await connection.execute('INSERT INTO users (fullname, email, username, password, imei, role_id) SELECT * FROM (SELECT "'+fullname+'","'+email+'","'+username+'","'+password+'","'+imei+'",'+role_id+') AS tmp WHERE NOT EXISTS (SELECT fullname, email, username, password, imei, role_id FROM users WHERE email = "'+email+'" or username = "'+username+'" or imei = "'+imei+'") LIMIT 1');
    
        connection.end();
        return [result];
    },
    update_login_time: async function(login_time,username){
        var connection = await this.get_connection();

        await connection.execute('UPDATE users SET last_login = "'+login_time+'" WHERE username = "'+username+'"');

        connection.end();
    },
    update_user_data: async function(fullname,email,username,imei,role_id){
        var connection = await this.get_connection();

        await connection.execute('UPDATE users SET fullname = "'+fullname+'", email = "'+email+'", imei = "'+imei+'", role_id = '+role_id+' WHERE username = "'+username+'"');
    
        connection.end();
    },
    update_user_password: async function(username,password){
        var connection = await this.get_connection();

        await connection.execute('UPDATE users SET password = "'+password+'" WHERE username = "'+username+'"');

        connection.end();
    },
    update_user_profile_pict: async function(username,profile_pict){
        var connection = await this.get_connection();

        await connection.execute('UPDATE users SET profile_pict = "'+profile_pict+'" WHERE username = "'+username+'"');

        connection.end();
    }
}