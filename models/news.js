var mysql_config = require('../config/database/mysql.json');

module.exports = {
    get_connection: async function(){
        var mysql = require('mysql2/promise');
        var bluebird = require('bluebird');
        
        var connection = await mysql.createConnection({
            host: mysql_config.host,
            user: mysql_config.user,
            password: mysql_config.password,
            database: mysql_config.database,
            Promise: bluebird
        });
        
        return connection;
    },
    get_news_data: async function(id){
        var connection = await this.get_connection();
        var [result] = [];
        if(id){
            [result] = await connection.execute('SELECT * FROM news WHERE news_id = '+id+'');
        }else{
            [result] = await connection.execute('SELECT * FROM news');
        }

        connection.end();
        return [result];
    },
    get_news_collapse: async function(limit){
        var connection = await this.get_connection();
        var [result] = await connection.execute('SELECT * FROM news LIMIT '+limit);

        connection.end();
        return [result];
    },
    insert_news_data: async function(title,subtitle,pict,description){
        var connection = await this.get_connection();

        var [result] = await connection.execute('INSERT INTO news (news_title, news_subtitle, news_pict, news_description) VALUES ("'+title+'", "'+subtitle+'", "'+pict+'", "'+description+'")');
        
        connection.end();
        return [result];
    },
    update_news_data: async function(id,title,subtitle,pict,description){
        var connection = await this.get_connection();

        if(pict){
            await connection.execute('UPDATE news SET news_title = "'+title+'", news_subtitle = "'+subtitle+'", news_pict = "'+pict+'", news_description = "'+description+'" WHERE news_id = '+id+'');
        }else{
            await connection.execute('UPDATE news SET news_title = "'+title+'", news_subtitle = "'+subtitle+'", news_description = "'+description+'" WHERE news_id = '+id+'');
        }
        
        connection.end();
    }
}