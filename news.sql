-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 10, 2019 at 10:12 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `news`
--

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `news_id` int(11) NOT NULL,
  `news_title` varchar(100) NOT NULL,
  `news_subtitle` longtext NOT NULL,
  `news_pict` varchar(100) NOT NULL,
  `news_description` longtext NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `news_title`, `news_subtitle`, `news_pict`, `news_description`, `created`, `updated`) VALUES
(1, 'Reenah', 'I am an outfit fairee', '/data/news/1558584839517.png', 'I am an outfit fairy', '2019-05-23 04:13:34', '2019-05-23 04:13:59'),
(2, 'Kerusuhan 22 Mei', 'Kerusuhan 22 Mei', '/data/news/1558585562757.png', 'Kerusuhan 22 Mei', '2019-05-23 04:26:03', '2019-05-23 04:26:03'),
(3, 'Kerusuhan 22 Mei', 'Kerusuhan 22 Mei', '/data/news/1558585591230.png', 'Kerusuhan 22 Mei', '2019-05-23 04:26:31', '2019-05-23 04:26:31'),
(4, 'Kerusuhan 22 Mei', 'Kerusuhan 22 Mei', '/data/news/1558585614726.png', 'Kerusuhan 22 Mei', '2019-05-23 04:26:54', '2019-05-23 04:26:54'),
(5, 'Nasi Goreng', 'Nasi Goreng', '/data/news/5.png', 'Kerusuhan 22 Mei', '2019-05-23 04:32:53', '2019-05-23 04:32:53');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`) VALUES
(1, 'Manager'),
(2, 'Staff');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `fullname` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `imei` varchar(12) NOT NULL,
  `profile_pict` varchar(100) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`fullname`, `email`, `username`, `password`, `imei`, `profile_pict`, `last_login`, `role_id`, `created`, `updated`) VALUES
('Anita Jessica Manosterino', 'anita@iglo.com', 'anitajessica', '$2b$10$Xu4A5JnJcle5UEweOnK0Xe2rvOXkNk6Lqh62DWq1KmqmKbWAehsha', '22922778779', '/data/profile_pict/anitajessica.png', '2019-06-10 08:09:44', 2, '2019-05-23 01:34:44', '2019-06-10 08:12:01'),
('Bella Hadid', 'bella@gmail.com', 'bellahadid', '$2b$10$Xu4A5JnJcle5UEweOnK0Xe2rvOXkNk6Lqh62DWq1KmqmKbWAehsha', '98922778909', NULL, NULL, 2, '2019-05-23 01:33:57', '2019-06-10 08:12:05'),
('Maria Kanati', 'mariakanati@gmail.com', 'mariakanati', '$2b$10$Xu4A5JnJcle5UEweOnK0Xe2rvOXkNk6Lqh62DWq1KmqmKbWAehsha', '22772178779', NULL, NULL, 2, '2019-05-24 05:57:52', '2019-06-10 08:12:08'),
('Marla Kanati', 'marlakanati@gmail.com', 'marlakanati', '$2b$10$Xu4A5JnJcle5UEweOnK0Xe2rvOXkNk6Lqh62DWq1KmqmKbWAehsha', '439088125677', NULL, NULL, 2, '2019-05-24 06:07:11', '2019-06-10 08:12:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
