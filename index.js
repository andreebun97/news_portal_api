const express = require('express');
const path = require('path');
const app = express();
const authController = require('./controllers/auth');
const userController = require('./controllers/user');
const newsController = require('./controllers/news');
const bodyParser = require('body-parser');
const port = 3100;

app.use(bodyParser.urlencoded({ extended : true }));
app.use(bodyParser.json());
app.use(express.static(__dirname));

app.use('/auth',authController);
app.use('/user',userController);
app.use('/news',newsController);

app.listen(port, function(){
    console.log('Server started on port '+port+'...');
});