var express = require('express')
var router = express.Router();
var userModel = require('../models/user');
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var isemail = require('isemail');
var convertToBase64 = require('base64-img');
var multer = require('multer');
var fs = require('fs');
var storage = multer.diskStorage({
    destination: function(req,file,cb){
        var mainDest = './data';
        var profilePictDest = './data/profile_pict'
        if(!fs.existsSync(mainDest)){
            fs.mkdirSync(mainDest,0777);
        }
        if(!fs.existsSync(profilePictDest)){
            fs.mkdirSync(profilePictDest,0777);
        }
        cb(null,profilePictDest);
    },
    filename: function(req, file, cb){
        cb(null, req.body.username + '.' + file.mimetype.split('/')[1]);
    }
});

var upload = multer({storage: storage});

var profilePictUpload = upload.fields([{'name':'profile_pict'}]);

router.use(async function(req,res,next){
    var responseMsg;
    if(req.headers.authorization == undefined){
        responseMsg = {
            error: -1,
            message: 'Error - Token cannot be null'
        }
        res.send(responseMsg);
    }else{
        var decodedJwt = jwt.verify(req.headers.authorization.split(' ')[1],'iglo',function(err,decoded){
            if(err){
                return false;
            }else{
                return decoded;
            }
        });

        if(decodedJwt == false){
            responseMsg = {
                error: -1,
                message: 'Error - Invalid token'
            }
            res.send(responseMsg);
        }else{
            next();            
        }
    }
});

router.post('/edit_profile', async function(req,res){
    var responseMsg;
    if(req.body.fullname == "" || req.body.email == "" || req.body.username == "" || req.body.imei == "" || req.body.role_id == ""){
        responseMsg = {
            error: -1,
            message: 'Error - Form input cannot be empty'
        }
    }else if(isemail.validate(req.body.email) == false){
        responseMsg = {
            error: -1,
            message: 'Error - Invalid email format'
        }
    }else{
        await userModel.update_user_data(req.body.fullname, req.body.email, req.body.username, req.body.imei, req.body.role_id);
        responseMsg = {
            error: 0,
            message: 'Success - Successfully updated data'
        }
    }
    
    res.send(responseMsg);
});

router.post('/change_password', async function(req,res){
    var responseMsg;
    var decodedJwt = jwt.verify(req.headers.authorization.split(' ')[1],'iglo',function(err,decoded){
        if(err){
            return false;
        }else{
            return decoded;
        }
    });
    if(req.body.old_password == "" || req.body.new_password == "" || req.body.confirm_new_password == ""){
        responseMsg = {
            error: -1,
            message: 'Error - Input form cannot be empty'
        }
    }else if(bcrypt.compareSync(req.body.old_password,decodedJwt.password) !== true){
        responseMsg = {
            error: -1,
            message: 'Error - Wrong old password'
        }
    }else if(req.body.new_password.localeCompare(req.body.old_password) == 0){
        responseMsg = {
            error: -1,
            message: 'Error - Please, change for password with different/unique string'
        }
    }else if(req.body.new_password.localeCompare(req.body.confirm_new_password) !== 0){
        responseMsg = {
            error: -1,
            message: 'Error - New password is not matched with confirm new password'
        }
    }else{
        await userModel.update_user_password(req.body.username,bcrypt.hashSync(req.body.new_password,0));
        responseMsg = {
            error: 0,
            message: 'Success - Password successfully changed'
        }
    }
    
    res.send(responseMsg);
});

router.post('/profile_pict', async function(req,res){
    profilePictUpload(req,res, async function(err){
        var responseMsg;
        var decodedJwt = jwt.verify(req.headers.authorization.split(' ')[1],'iglo',function(err,decoded){
            if(err){
                return false;
            }else{
                return decoded;
            }
        });

        if(req.body.username.localeCompare(decodedJwt.username) !== 0){
            fs.unlinkSync(req.files.profile_pict[0].destination + '/' + req.files.profile_pict[0].filename);
            responseMsg = {
                error: -1,
                message: 'Error - Invalid username'
            }
        }else{
            if(err){
                responseMsg = {
                    error: -1,
                    message: 'Error - Profile picture cannot be uploaded'
                }
            }else{
                var convertedProfilePict = convertToBase64.base64Sync(req.files.profile_pict[0].destination + '/' + req.files.profile_pict[0].filename);
                console.log(convertedProfilePict);
                var profilePictDetail = {
                    destination: req.files.profile_pict[0].destination.substring(1) + '/' + req.files.profile_pict[0].filename,
                    base64img: convertedProfilePict
                }
                await userModel.update_user_profile_pict(req.body.username,profilePictDetail.destination);
                responseMsg = {
                    error: 0,
                    message: 'Success - Profile picture can be uploaded',
                    data: profilePictDetail
                }
            }    
        }
        
        res.send(responseMsg);
    });
});

module.exports = router;