var express = require('express')
var router = express.Router();
var authModel = require('../models/user');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var isemail = require('isemail');
var nodemailer = require('nodemailer');
var sender = require('../config/mail/sender.json');

router.post('/login', async function(req,res){
    var responseMsg;
    console.log(req.headers.authorization);
    var [result] = await authModel.get_user_data(req.body.username,null,null);
    var today = new Date();
    var month = (today.getMonth()+1);
    var day = today.getDate();
    if(month < 10){
        month = "0"+month;
    }
    if(day < 10){
        day = "0"+day;
    }
    var date = today.getFullYear()+'-'+month+'-'+day;
        
    var hour = today.getHours();
    var minute = today.getMinutes();
    var second = today.getSeconds();
    if(hour < 10){
        hour = "0" + hour;
    }
    if(minute < 10){
        minute = "0"+minute;
    }
    if(second < 10){
        second  = "0"+second;
    }
    var time = hour + ":" + minute + ":" + second;
    var dateTime = date+' '+time;
    console.log(dateTime);
    if(result.length > 0 && bcrypt.compareSync(req.body.password,result[0].password) == true){
        if(req.headers.authorization == undefined){
            var passwordTemp = bcrypt.hashSync(req.body.password,0);
            var token = jwt.sign({username: req.body.username, password: passwordTemp},
                'iglo',{expiresIn: '24h'}
            );
            await authModel.update_login_time(dateTime,req.body.username);
            responseMsg = {
                error: 0,
                message: 'Success - Login',
                data:{
                    token: token,
                    user: {
                        username: result[0].username,
                        fullname: result[0].fullname,
                        role_name: result[0].role_name
                    }
                }
            }
        }else{
            var decodedJwt = jwt.verify(req.headers.authorization.split(' ')[1],'iglo',function(err,decoded){
                if(err){
                    return false;
                }else{
                    return decoded;
                }
            });

            if(decodedJwt == false){
                responseMsg = {
                    error: -1,
                    message: 'Error - Invalid token'
                }
            }else if(req.body.username.localeCompare(decodedJwt.username) !== 0 || bcrypt.compareSync(req.body.password,decodedJwt.password) == false){
                responseMsg = {
                    error: -1,
                    message: 'Error - Wrong username/password'
                }
            }else{
                var token = jwt.sign({username: req.body.username, password: passwordTemp},
                    'iglo',{expiresIn: '24h'}
                );
                await authModel.update_login_time(dateTime,req.body.username);
                responseMsg = {
                    error: 0,
                    message: 'Success - Login',
                    data:{
                        token: token,
                        user: {
                            username: result[0].username,
                            fullname: result[0].fullname,
                            role_name: result[0].role_name
                        }
                    }
                }
            }
        }
    }else{
        responseMsg = {
            error: -1,
            message: 'Error - Wrong username/password'
        }
    }
    
    res.send(responseMsg);
});

router.post('/forgot_password', async function(req,res){
    var responseMsg;
    var [result] = await authModel.get_user_data(req.body.username);
    
    if(req.body.username == ""){
        responseMsg = {
            error: -1,
            message: 'Error - Input form cannot be empty'
        };
        res.send(responseMsg);
    }else if(result.length == 0){
        responseMsg = {
            error: -1,
            message: 'Error - No username found!'
        }
        res.send(responseMsg);
    }else{
        var alternativePassword = Math.random().toString(36).substring(2, 15);
        var alternativePasswordBcrypt = bcrypt.hashSync(alternativePassword,0);
        var token = jwt.sign({username: req.body.username, password: alternativePasswordBcrypt},
            'iglo',{expiresIn: '24h'}
        );

        var transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            auth: {
                user: sender.user,
                pass: sender.pass
            }
        });
        var fullUrl = '//' + req.get('host') + req.originalUrl + '/' + req.body.username + '?token=' + token ;
        
        var mailOptions = {
            to: result[0].email,
            subject: 'Reset Account Password',
            html: '<div class="form-detail"><div class="username-detail"><p>Here is an email confirmation that the password for username <b>'+req.body.username+'</b> has just been changed.</p></div><div class="password-detail"><label>Password: </label><label>'+alternativePassword+'</label></div><div class="link-detail"><p>Click <a href="'+fullUrl+'">this link</a> to change your password.</p></div><div class="expired-token-detail"><p>This link will be expired in 24 hours.</p></div></div>'
        }

        transporter.sendMail(mailOptions, (error, info) => {
            if(error){
                return console.log(error);
            }else{
                console.log(fullUrl);
                console.log('Message %s sent: %s', info.messageId, info.response);
                responseMsg = {
                    error: 0,
                    message: 'Success - The link has been sent to your email:'+result[0].email,
                    data: {
                        token: token,
                        user: {
                            username: req.body.username,
                            password: alternativePassword
                        }
                    }
                }
                res.send(responseMsg);    
            }
            
        })
        
    }
});

router.get('/forgot_password/:username', async function(req,res){
    var responseMsg;
    var [result] = await authModel.get_user_data(req.params.username);
    var decodedJwt = jwt.verify(req.query.token,'iglo',function(err,decoded){
        if(err){
            return false;
        }else{
            return decoded;
        }
    });

    if(result.length == 0){
        responseMsg = {
            error: -1,
            message: 'Error - Data cannot be found!'
        }
    }else if(decodedJwt == false){
        responseMsg = {
            error: -1,
            message: 'Error - Invalid token!'
        }
    }else{
        console.log(req.query.token);
        console.log(decodedJwt);
        await authModel.update_user_password(req.params.username,decodedJwt.password);
        responseMsg = {
            error: 0,
            message: 'Success - Password has been changed'
        }
    }
    
    res.send(responseMsg);
});


router.post('/register', async function(req,res){
    var responseMsg;
    if(req.body.username == "" || req.body.email == "" || req.body.username == "" || req.body.password == "" || req.body.confirm_password == "" || req.body.imei == "" || req.body.last_login == "" || req.body.role_id == ""){
        responseMsg = {
            error: -1,
            message: 'Error - Input form cannot be empty'
        }
    }else if(isemail.validate(req.body.email) == false){
        responseMsg = {
            error: -1,
            message: 'Error - Invalid email format'
        }
    }else if(req.body.password.includes(req.body.username) == true || req.body.password.includes(req.body.fullname) == true || req.body.password.includes(req.body.email) == true){
        responseMsg = {
            error: -1,
            message: 'Error - Email cannot contain the form input'
        }
    }else if(req.body.password.localeCompare(req.body.confirm_password) !== 0){
        responseMsg = {
            error: -1,
            message: 'Error - Password must be matched with confirm password'
        }
    }else{
        var [result] = await authModel.insert_user_data(req.body.fullname,req.body.email,req.body.username,bcrypt.hashSync(req.body.password,0),req.body.imei,req.body.role_id);
    
        if(result.insertId == 0){
            responseMsg = {
                error: -1,
                message: 'Error - Duplicate Username/Imei/Email!'
            }
        }else{
            responseMsg = {
                error: 0,
                message: 'Success - Data has been successfully inserted!'
            }
        }
    }

    res.send(responseMsg);
});

module.exports = router;