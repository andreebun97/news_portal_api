var express = require('express')
var router = express.Router();
var newsModel = require('../models/news');
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var nodemailer = require('nodemailer');
var isemail = require('isemail');
var convertToBase64 = require('base64-img');
var multer = require('multer');
var fs = require('fs');
var storage = multer.diskStorage({
    destination: function(req,file,cb){
        var mainDest = './data';
        var newsDest = './data/news'
        if(!fs.existsSync(mainDest)){
            fs.mkdirSync(mainDest,0777);
        }
        if(!fs.existsSync(newsDest)){
            fs.mkdirSync(newsDest,0777);
        }
        cb(null,newsDest);
    },
    filename: function(req, file, cb){
        cb(null, Date.now() + '.' + file.mimetype.split('/')[1]);
    }
});

var upload = multer({storage: storage});

var newsUpload = upload.fields([{'name':'news_pict'}]);

router.use(async function(req,res,next){
    var responseMsg;
    if(req.headers.authorization == undefined){
        responseMsg = {
            error: -1,
            message: 'Error - Token cannot be null'
        }
        res.send(responseMsg);
    }else{
        var decodedJwt = jwt.verify(req.headers.authorization.split(' ')[1],'iglo',function(err,decoded){
            if(err){
                return false;
            }else{
                return decoded;
            }
        });

        if(decodedJwt == false){
            responseMsg = {
                error: -1,
                message: 'Error - Invalid token'
            }
            res.send(responseMsg);
        }else{
            next();            
        }
    }
});

router.get('/get_news', async function(req,res){
    var responseMsg;
    var newsArray = [];
    var [result] = await newsModel.get_news_data();
    for (var b = 0; b < result.length; b++) {
        var newsPerIdx = {
            id: result[b].news_id,
            news_title: result[b].news_title,
            news_subtitle: result[b].news_subtitle,
            news_pict: result[b].news_pict,
            news_description: result[b].news_description
        }

        newsArray.push(newsPerIdx);        
    }

    responseMsg = {
        error: 0,
        message: 'Success - Get news data',
        data: newsArray.length == 0 ? null : newsArray
    }

    res.send(responseMsg);
});

router.get('/get_news/:id', async function(req,res){
    var responseMsg;
    var newsArray = [];
    var [result] = await newsModel.get_news_data(req.params.id);
    for (var b = 0; b < result.length; b++) {
        var newsPerIdx = {
            id: result[b].news_id,
            news_title: result[b].news_title,
            news_subtitle: result[b].news_subtitle,
            news_pict: result[b].news_pict,
            news_description: result[b].news_description
        }

        newsArray.push(newsPerIdx);        
    }

    responseMsg = {
        error: 0,
        message: 'Success - Get news data by ID',
        data: newsArray.length == 0 ? null : newsArray
    }

    res.send(responseMsg);
});

router.post('/get_news_collapse', async function(req,res){
    var responseMsg;
    var newsArray = [];
    var [result] = await newsModel.get_news_collapse(req.body.limit);
    for (var b = 0; b < result.length; b++) {
        var newsPerIdx = {
            id: result[b].news_id,
            news_title: result[b].news_title,
            news_subtitle: result[b].news_subtitle,
            news_pict: result[b].news_pict,
            news_description: result[b].news_description
        }

        newsArray.push(newsPerIdx);        
    }

    responseMsg = {
        error: 0,
        message: 'Success - Get news data collapse',
        data: newsArray.length == 0 ? null : newsArray
    }

    res.send(responseMsg);
})

router.post('/insert_news', async function(req,res){
    newsUpload(req,res, async function(){
        var responseMsg;
        var decodedJwt = jwt.verify(req.headers.authorization.split(' ')[1],'iglo',function(err,decoded){
            if(err){
                return false;
            }else{
                return decoded;
            }
        });

        if(req.body.username.localeCompare(decodedJwt.username) !== 0){
            fs.unlinkSync(req.files.news_pict[0].destination + '/' + req.files.news_pict[0].filename);
            responseMsg = {
                error: -1,
                message: 'Error - Wrong username'
            }
        }else if(req.body.news_title == "" || req.body.news_subtitle == "" || req.body.news_description == ""){
            fs.unlinkSync(req.files.news_pict[0].destination + '/' + req.files.news_pict[0].filename);
            responseMsg = {
                error: -1,
                message: 'Error - Form input cannot be empty'
            }
        }else{
            var [result] = await newsModel.insert_news_data(req.body.news_title, req.body.news_subtitle, req.files.news_pict[0].destination.substring(1) + '/' + req.files.news_pict[0].filename, req.body.news_description);
            
            fs.renameSync(req.files.news_pict[0].destination + '/' + req.files.news_pict[0].filename,req.files.news_pict[0].destination + '/' + result.insertId + '.' + req.files.news_pict[0].mimetype.split('/')[1]);
            var convertedNewsPict = convertToBase64.base64Sync(req.files.news_pict[0].destination + '/' + result.insertId + '.' + req.files.news_pict[0].mimetype.split('/')[1]);
            var newsPictDetail = {
                destination: req.files.news_pict[0].destination.substring(1) + '/' + result.insertId + '.' + req.files.news_pict[0].mimetype.split('/')[1],
                base64img: convertedNewsPict
            }
            await newsModel.update_news_data(result.insertId,req.body.news_title, req.body.news_subtitle, newsPictDetail.destination, req.body.news_description);
            responseMsg = {
                error: 0,
                message: 'Success - News data has been successfully inserted',
                data: newsPictDetail
            }
        }

        res.send(responseMsg);
    });
});

router.post('/update_news/:id', async function(req,res){
    newsUpload(req,res, async function(error){
        var responseMsg;
        var decodedJwt = jwt.verify(req.headers.authorization.split(' ')[1],'iglo',function(err,decoded){
            if(err){
                return false;
            }else{
                return decoded;
            }
        });

        if(req.body.username.localeCompare(decodedJwt.username) !== 0){
            fs.unlinkSync(req.files.news_pict[0].destination + '/' + req.files.news_pict[0].filename);
            responseMsg = {
                error: -1,
                message: 'Error - Wrong username'
            }
        }else if(req.body.news_title == "" || req.body.news_subtitle == "" || req.body.news_description == ""){
            fs.unlinkSync(req.files.news_pict[0].destination + '/' + req.files.news_pict[0].filename);
            responseMsg = {
                error: -1,
                message: 'Error - Form input cannot be empty'
            }
        }else{
            if(Object.keys(req.files).length == 0){
                await newsModel.update_news_data(req.params.id,req.body.news_title, req.body.news_subtitle, req.body.news_description);
                responseMsg = {
                    error: 0,
                    message: 'Success - News data has been successfully updated'
                }
            }else{
                var [result] = await newsModel.get_news_data(req.params.id);
                fs.unlinkSync('.'+result[0].news_pict);
                var convertedNewsPict = convertToBase64.base64Sync(req.files.news_pict[0].destination + '/' + req.files.news_pict[0].filename);
                console.log(convertedNewsPict);
                var newsPictDetail = {
                    destination: req.files.news_pict[0].destination.substring(1) + '/' + req.files.news_pict[0].filename,
                    base64img: convertedNewsPict
                }
                await newsModel.update_news_data(req.params.id,req.body.news_title, req.body.news_subtitle, newsPictDetail.destination, req.body.news_description);
                responseMsg = {
                    error: 0,
                    message: 'Success - News data has been successfully updated',
                    data: newsPictDetail
                }
            }
            
        }

        res.send(responseMsg);
    });
});

module.exports = router;